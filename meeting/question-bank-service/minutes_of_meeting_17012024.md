# Minutes Of Meeting
**Date: 17th Jan 2024
Time: 4:00 pm
Topic: Introduction and Initial Tasks**

# Attendees
**Clients**
---
* Raj Agnihotri
* Priya Raman
* Ravi Kiran

**TA**
---
* Divij D

**Team Members**
---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Ansh Chablani | ansh.chablani@research.iiit.ac.in | 2022111031 |
| Divyarajsinh Mahida | divyarajsinh.mahida@students.iiit.ac.in | 2022101085 |
| Shreyas Badami | shreyas.badami@research.iiit.ac.in | 2021101018 |
| Siddharth Singh | siddharth.s@research.iiit.ac.in | 2021113001 |

**Outline**
---
| Type | Description | Owner | Deadline |
| ---- | ----------  | ----  | -------- |
| I | Introduction to Virtual Labs, what it is and the analytics for the virtual labs website | na | na |
| I | Introduction to VLEAD, which focuses on maintaining the virtual labs platform | na | na |
| I | Benefits of working with Virtual Labs as college sophomores | na | na |
| I | Slack was introduced as the main form of communication between the client and the dev team, the dev team was informed to use slack to communicate with the client at any time | na | na |
| I | Dev team was informed that at each meeting a slide deck had to be presented with 4 slides with the following information on it - details of last meet, progress made over the last week, next steps and development issues. The team was also informed to keep work logs. | na | na |
| I | We were also told (shortly after the meeting) to divide our team into 2 - one handling frontend and one backend. | na | na |
| I | Motivation behind the project i.e. to create an open platform for users to add and extract questions | na | na |
| I | The goal of the project is to make a question bank service that would facilitate its users to add questions and extract them. This extraction and appending of the questions would be done using a credit based system. Additionally there would be several features for the implementation of several governing policies like question plagiarism checker, validation, searching based on tags ensuring the quality of the system. Moreover, a user authentication feauture should also be added. | na | na |
| T | To read the reference documents available on the 2024-dass slack | Dev Team | 24-01-2023 |
| D | Weekly Meeting Time - Fridays 4:00pm  | na | na |

More key points discussed:
- It should be a cloud-souces question bank.
- We should be able to access the service using UI and an API - which would have to be developed.
- There should be some incentive required to extract a question.
- Default authentication should one's Google ID (preferably)
- We would be using firebase.
- Every week there should preferably be a demo.
- JS is the preferred language for backend.
- On GitLab we have to directly work on the main branch.
- And in the development branch of GitHub

