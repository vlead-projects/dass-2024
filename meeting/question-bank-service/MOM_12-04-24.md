# Minutes Of Meeting
**Date: 12th Apr 2024
Time: 4:00 pm
Topic: Weekly Report and Progress Discussion **

# Attendees
**Clients**
---
* Raj Agnihotri
* Priya Raman
* Ravi Kiran

**Team Members**
---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Ansh Chablani | ansh.chablani@research.iiit.ac.in | 2022111031 |
| Divyarajsinh Mahida | divyarajsinh.mahida@students.iiit.ac.in | 2022101085 |
| Shreyas Badami | shreyas.badami@research.iiit.ac.in | 2021101018 |
| Siddharth Singh | siddharth.s@research.iiit.ac.in | 2021113001 |

**Outline**
---
| Type | Description | Owner | Deadline |
| ---- | ----------  | ----  | -------- |
| I | Presentation of the week's work done and Progress | na | na |
| I | Displaying of work done on the image upload till date and the enhanced RESPONSIVENESS of project| na | na |
| I | Client's response on question whether they want IMAGE UPLOAD for ALL Questions or not| na | na |
| I | Display of work on API call and how different applications can call the API| na | na |
| I | Client's question on how authorization of the API call is working and suggestions for the same| na | na |
| I | Client's remark that if we have time for the project , the only priority FEATURE should be the authorization to the API calls | Dev Team | 18th Apr |
| I | Dev Team's remark that we don't have a lot of deadlines in upcoming week so can manage multiple things|na| na |
| I | Client's remark we should focus on wrapping and documentation of the project|na| na |
| I | Client's remark we should debug and make run existing codes|na| na |
| I | Also focus on creation of BLOGS of the project | na | na |
| I | Also include the things we tried and couldn't achieve and failures in documentation as well as blogs| na | na |
| I | Face to Face Meeting with client-Priya Raman next week| ALL | 16th Apr |
