# Minutes Of Meeting
**Date: 20th Jan 2023
Time: 4:30 pm
Topic: Introduction and Initial Tasks**

# Attendees
**Clients**
---
* Raj Agnihotri
* Priya Raman

**TA**
---
* Karmanjyot Singh

**Team Members**
---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Haran Raajesh | haran.raajesh@students.iiit.ac.in | 2021101005 |
| Akshat Sanghvi | akshat.sanghvi@students.iiit.ac.in | 2021101094 |
| Keshav Gupta | iamkeshav06@gmail.com | 2021101018 |
| Sreeram Reddy Vennam | vennam404@gmail.com | 2021101045 |

**Outline**
---
| Type | Description | Owner | Deadline |
| ---- | ----------  | ----  | -------- |
| I | Introduction to Virtual Labs, what it is and the analytics for the virtual labs website | na | na |
| I | Introduction to VLEAD, which focuses on maintaining the virtual labs platform | na | na |
| I | Benefits of working with Virtual Labs as college sophomores | na | na |
| I | Slack was introduced as the main form of communication between the client and the dev team, the dev team was informed to use slack to communicate with the client at any time | na | na |
| I | Dev team was informed that at each meeting a slide deck had to be presented with 3 slides with the following information on it - details of last meet, progress made over the last week, next steps. The team was also informed to keep work logs. | na | na |
| I | Motivation behind the project i.e. to optimise AWS usage and user experience | na | na |
| I | The goal of the project is to take a subset of the labs and convert them into a PWA (Progressive Web Application). Further subsequent tasks include converting the PWA into an Android App and deploying on the Play Store followed by doing the same for the Microsoft Windows Store. | na | na |
| T | To read the reference documents available on the DASS-2023 slack | Dev Team | 27-01-2023 |
| T | To build a simple one page PWA | Dev Team | 27-01-2023 |
| D | Weekly Meeting Time - Fridays 4:00pm  | na | na |
