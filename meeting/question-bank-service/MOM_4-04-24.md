# Minutes Of Meeting
**Date: 4th Apr 2024
Time: 6:30 pm
Topic: Weekly Report and Progress Discussion **

# Attendees
**Clients**
---
* Raj Agnihotri
* Priya Raman
* Ravi Kiran

**Team Members**
---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Ansh Chablani | ansh.chablani@research.iiit.ac.in | 2022111031 |
| Divyarajsinh Mahida | divyarajsinh.mahida@students.iiit.ac.in | 2022101085 |
| Shreyas Badami | shreyas.badami@research.iiit.ac.in | 2021101018 |
| Siddharth Singh | siddharth.s@research.iiit.ac.in | 2021113001 |

**Outline**
---
| Type | Description | Owner | Deadline |
| ---- | ----------  | ----  | -------- |
| I | Presentation of the week's work done and Progress | na | na |
| I | Displaying of DEBUG work done on the image upload till date| na | na |
| I | Displaying of DEBUG work done on the Markdown input option| na | na |
| I | Remark by Dev Team that only so much work completed during the week due to QUIZZES entrie week| na | na |
| I | Client's aggrement to justification and remark that we should now focus on the SCOPING of the project| na | na |
| I | Client's remark that we don't need to add a new functionality to project , just the wrapping up of project | na | na |
| I | Client's remark we should debug and make run existing codes|na| na |
| I | Also focus on documentation of the project | na | na |
| I | Also include the things we tried and couldn't achieve and failures in documentation| na | na |



