# Minutes Of Meeting
**Date: 15th Mar 2024
Time: 4:00 pm
Topic: Weekly Report and Progress Discussion **

# Attendees
**Clients**
---
* Raj Agnihotri
* Priya Raman
* Ravi Kiran

**Team Members**
---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Ansh Chablani | ansh.chablani@research.iiit.ac.in | 2022111031 |
| Divyarajsinh Mahida | divyarajsinh.mahida@students.iiit.ac.in | 2022101085 |
| Shreyas Badami | shreyas.badami@research.iiit.ac.in | 2021101018 |
| Siddharth Singh | siddharth.s@research.iiit.ac.in | 2021113001 |

**Outline**
---
| Type | Description | Owner | Deadline |
| ---- | ----------  | ----  | -------- |
| I | Presentation of the week's work done and Progress | na | na |
| I | Displaying of work done on refinements of all the screens done| na | na |
| I | This includes Search question screen(addition of remove option in difficulty),  Add Question Screen(Removed inconsistenciesn with search question screen regarding tags), Question display screen(addition of tags,difficulty,correct answer and remove from downloads option)| na | na |
| I | Client's appreciation on the work done regarding remove from downloads| na | na |
| T | Client's remark that they would do scoping of project from next week| Client | 22nd March |
| I | Client's remark to keep difficulty of questions on top of screen| na | na |
| I | Also that question appears too far to the left of screen|na| na |
| I | Convincing by dev team to keep it to left as app is more mobile friendly | na | na |
| I | Client's explanation of how to implement separation of bbackend and frontend using Dumb API and just hard code its first implementation|na|na
| I | Dev Team's doubt whether to keep display results on separate page and not exactly on search| na | na |
| I | Client's suggestion not to overwork and plan project execution accordingly as only a month left now| na | na |
| I | Client's suggestion we can rather bookmark some pages of search which user may visit frequently| na | na |
| I | Client's remark that the UI is bit blank and we need to improve but not urgent . First priority is correct working of all functionalities| na | na |
| I | Client's suggestion to focus on backend for next week and code separation| Dev Team | 22nd March  |
| I | Client's remark that it will require some time to address security issues of user calling via API. We need to authenticate user before that| na | na |
| I | Client's clarity on how to separate frontend and backend and also closing remark about what its use is| na | na |