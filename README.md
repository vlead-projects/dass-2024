# DASS-2024


## Introduction 
  This document serves as a dashboard of the 2024 - DASS projects.
 
## Name of the Company
  Virtual Labs Engineering and Architecture Design

## Primary Work Location
  Online

## Email
  dass-2024@vlabs.ac.in

## Projects Proposed

  - Project-1 - VScode-Web-Extension

  - Project-2 - Question-Bank-Service


## Project- 1 - VScode-Web-Extension

  ### Description
- To enhance the Virtual Labs VS Code extension by:
  - Enabling complete functionality within a web browser
  - Addressing existing bugs and limitations
  - Streamlining the experiment development lifecycle
  - Automating release processes
 
  ### Skill Set 
**Frontend:**
- TypeScript
- HTML
- CSS

**Backend:**
- GitHub API client library

**Integration:**
- VS Code web extension API
- GitHub API
 
  ### Features
- Easy, intuitive interface compatible with vscode.dev
- Simultaneous support for multiple users
- Comprehensive experiment development lifecycle support, including:
  - Cloning repositories
  - Code and data validation
  - Local building and deployment
  - Artifact cleaning
  - Testing branch merging and deployment triggering
  - Pull request raising
- Fully automated release process
- GitHub authentication

 
  ### Version No 1   
  The tasks to be completed are:
  - Porting the existing VS Code extension to a web extension
  - Documentation and workarounds for functionality that cannot be ported
  - Integration of Markdown editor with the web IDE
  - Packaging and delivery of the complete functionality as a single unit
 
  ### Outcome 
  The outcome of the first version will be a VS Code Web Extension, which will be invoked from the browser and will provide a possibly reduced set of functionality than the existing offline extension. All the limitations, workarounds and planned solutions will be clearly documented.
 
  ### Version No 2
  
   
  ### Outcome
  

## Number of People
- 4
  ## Existing Documents
- [Link](https://docs.google.com/document/d/1vMwzNcEaBrhvvoZlplc5OPlHvrnpr1zXRo-UAhSKQ6g/edit?usp=drive_link) to Client Requirement Document
- [Link](https://github.com/virtual-labs/tool-vscode-plugin/blob/main/README.md) to the documentation of existing VSCode Extention  

## Project-2 - Question-Bank-Service

  ### Description
- To create a centralized repository of multiple-choice questions that can be accessed and contributed to by authorized users.
- To facilitate question retrieval based on search terms, tags, and similarity matching.

  ### Skill Set 
- **Programming language:** Node.js or Python
- **Database:** Document database (e.g., MongoDB, Couchbase, Firestore)
- **Frontend:** Responsive UI framework (e.g., React, Svelte, Vue.js)
- **Backend:** Serverless architecture (e.g., AWS Lambda, Google Cloud Functions)
- **Additional tools:** Plagiarism checker, similarity search library

  ### Features
- Question submission and validation against a defined schema
- Plagiarism checking
- Support for text, images, and equations in questions and options
- User authorization with different roles (retriever, submitter, admin)
- Reward system for question submission
- Credit-based retrieval
- Question ranking
- Multidimensional classification using tags
- Automatic tag suggestion
- Similarity search
- UI and API access for individual and bulk operations



  ### Version 1   
  The tasks to be completed for version 1 are:
  - Definition of the question JSON schema
  - Building question submission screen
  - Building quetion retrieval screen
  - Implementation of user auth
  - Configuration of the Database
  - Building CRUD API for questions
  - Deployment of the Front End on a static site host
  - Deployment of the API on a Serverless Framework
   
  ### Outcome 
  The outcome of the first version is a deployed app with the basic feature set of the question bank service. The service should allow submission and retrieval of multiple choice questions after validation against the schema.
  

  ### Version 2
  
  ### Outcome

 ## Number of People
- 4
  
  ## Existing Documents
  - [Link](https://docs.google.com/document/d/111voUNY0b-IWggkNwoviegLSh9L2m6bdJa8H_7HUb2c/edit?usp=drive_link) to Client Requirements Document 

## Team Members and Contact Information  

  TA - Divij, divij.d@students.iiit.ac.in


| SNo  |Team ID   |Members in the team   | Email address/Phone Number  | Github Handle/Gitlab Handle  | Project Repo Link| Hosted Link|
|---|---|---|---|---|---|---|
| 1. |  Project1(Team 11) |Namrata Baliga,Nishita Kannan, Adari Dileepkumar, Pinninti Pavitra  | namrata.baliga@students.iiit.ac.in, nishita.kannan@students.iiit.ac.in, dileepkumar.adari@students.iiit.ac.in, pavitra.pinninti@research.iiit.ac.in | NamsB7<br> 112Nisha<br> Dileepadari<br> PavitraPi<br> |  |  |  
| 2.| Project2(Team 21)| Ansh Chablani , Shreyas Badami , Siddharth Singh, Divyarajsinh Rajendrasinh Mahida| ansh.chablani@research.iiit.ac.in, shreyas.badami@research.iiit.ac.in, siddharth.s@research.iiit.ac.in, divyarajsinh.mahida@students.iiit.ac.in |anshium<br> Shreyas-Badami<br> siddharthsingh250304<br> Divyaraj-coder-create<br> |   |   |


  
## Working Guidelines 

### Development Process 

Step 1 : Choose one exp from the assigned list and send a mail to systems@vlabs.ac.in with your Github handle and associated email id requesting to be added to the repository.  

Step 2 : Clone the repository.

Step 3 : Populate the dev branches of the repository with the source code of the experiments and unit test the experiments locally. Do not delete gh-pages branch. This is required for automatically deploying the experiment along with UI on GitHub pages for testing the complete experiment .Also do not delete .github directory and the LICENSE file.

Step 4 : Merge the fully tested dev branch to testing branch. This will automatically deploy the experiment along with UI on GitHub pages for testing the complete experiment. You will receive an email about the success/failure of the deployment on the email id associated with the github handle provided in Step 1. 

Step 5 : Set up a demo with the Virtual Labs Team and merge the code with **main branch** only after getting approval from them. 


### Realization in terms of Milestones
  Follow [agile](https://en.wikipedia.org/wiki/Agile_software_development) software development and use [scrum](https://www.scrumalliance.org/why-scrum)  methodology to incrementally working software.  Every project has a product backlog.  In each sprint, items from the product backlog are picked to be realized in a milestone.  Each item from a product backlog translates to one or more tasks and each task is tracked as an issue on
  github.  A milestone is a collection of issues.  It is upto the mentor and the team to choose either one or two week sprints.

  Each release is working software and a release determines the achievement of a milestone.  A project is realized as a series of milestones.

  This planning will be part of the master repo of respective project repos.
  
### Communication 
  Every discussion about the project will be through issues.  Mails are not used for discussion.
  Any informal communication will be through Slack - 2024-dass channel.

### Total person hours  ( As committed by the course coordinator in 2020) 
   Design and Analysis of Software Systems (DASS, formerly SSAD) course for the 2nd year students to work on. As you know, the project component carries 40% weightage for the course, and it would be great if the students would get an opportunity to work with you and the team as their
   clients.

   Each student expected to devote 10 hours per week for their projects. It is not 10 hours per team, rather 10 hours per student. Just confirmed it with Prof. Ramesh. There are a total of 12 productive weeks that the student must work for the project. (3 in January, 3 in February, 4 in March and 2 in April). This is excluding the mid-semester week, as that wouldn't be a productive one. There would be 4 members per team, so each team devotes
   40 hours per week and 480 hours in total for the project.

   Since these project are meant to simulate a proper industrial project, they would have to sincerely meet with the clients regularly, document everything, keep track of their status on git and most importantly complete the project and get a good client feedback, else they'd get a bad grade! Kindly requesting you to open up some Virtual Labs projects for the students to work on.
   
### Things done at the weekly meeting

  1. Every meeting starts with a presentation of the code documents and a demonstration.

  2. The pull/merge request is reviewed at the meeting.

  3. Task planning for the next week is discussed.  The students take the meeting notes.  The meeting notes has
     the below structure.  From the meeting, action items are thrashed out and are listed.  The mom file is also part of this repository.  Sample mom is placed under [meetings](https://gitlab.com/vlead-projects/dass-2024/-/tree/main/meeting) directory. You may also refer to the [mom](https://gitlab.com/vlead-projects/experiments/2018-ssad/index/blob/master/src/meetings/2018-08-31-mom.org)
     which captures the mom of all the teams. 

   
     ** Agenda

     ** Discussion Points

     ** Action

     |   |   |   |   |   
     |---|---|---|---| 
     | Item  |  Issue | Artifact   |Status    |   
     | Implement filter|Link to| link to file|In progress|issue |in the repo 


### Work logs
  The work logs of each student will need to be pushed to the repository shared by the TA/Course Coordinator. The work-logs will consider for Grading. 

### Team Meetings
  Team meetings will be held every Wednesday from 4:00 PM till 5:00 PM with a slotted time of 30 min per team.
  


